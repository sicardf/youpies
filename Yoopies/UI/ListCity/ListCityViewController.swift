//
//  ViewController.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import UIKit

class ListCityViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private var viewModel: ListCityViewModel! {
        didSet {
            self.viewModel.listCityDidChange = { [unowned self] viewModel in
                self.tableViewStyleLine(viewModel.listCityViewModelItem.count)
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                            target: self,
                                                            action: #selector(ListCityViewController.segueAddCity))
        viewModel = ListCityViewModel()
        configTableView()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewModel.requestCodeData()
    }
 
    private func configTableView() {
        tableView?.dataSource = viewModel
        tableView?.delegate = self
        tableView?.keyboardDismissMode = .onDrag
        tableView?.estimatedRowHeight = 100
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: "cityCell")
        tableView?.separatorStyle = .none
        
        let messageTB = UILabel(frame: CGRect(x: 0, y: 0, width: (tableView?.bounds.size.width)!, height: (tableView?.bounds.size.height)!))
        messageTB.text = "Merci d'ajouter une ville en favoris"
        messageTB.textAlignment = .center
        messageTB.sizeToFit()
        tableView?.backgroundView = messageTB
    }
    
    @objc func segueAddCity() {
        self.performSegue(withIdentifier: "AddCitySegue", sender: self)
    }
    
}

extension ListCityViewController: UITableViewDelegate {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CityWeatherSegue" {
            let cityWeatherViewController = segue.destination as! CityWeatherViewController
            let indexPath = sender as! IndexPath
            cityWeatherViewController.city = viewModel.listCityViewModelItem[indexPath.row]
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "CityWeatherSegue", sender: indexPath)
    }
    
    func tableViewStyleLine(_ countItem: Int) {
        if countItem == 0 {
            self.tableView?.separatorStyle = .none
            tableView?.backgroundView?.isHidden = false
        } else {
            self.tableView.separatorStyle = .singleLine
            tableView?.backgroundView?.isHidden = true
        }
    }
}


