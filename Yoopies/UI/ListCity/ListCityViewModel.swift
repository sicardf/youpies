//
//  ListWeatherViewModel.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListCityViewModel: NSObject {
    
    private var _listCityViewModelItem = [CityViewModelItem]() {
        didSet {
            self.listCityDidChange?(self)
        }
    }
    public var listCityViewModelItem: [CityViewModelItem] {
        get {
            return _listCityViewModelItem
        }
    }
    
    var listCityDidChange: ((ListCityViewModel) -> ())?
    
    func requestCodeData() {
        _listCityViewModelItem = []
        if let listCity = DataCity().allCity() {
            for i in listCity {
                let city = CityViewModelItem(description: i.descriptionName ?? "")
                self._listCityViewModelItem.append(city)

            }
        }
    }

}

extension ListCityViewModel: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCityViewModelItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cityCell") as UITableViewCell!
        cell.textLabel?.text = listCityViewModelItem[indexPath.row].description
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DataCity().delete(description: listCityViewModelItem[indexPath.row].description)
            requestCodeData()
        }
    }
    
}

