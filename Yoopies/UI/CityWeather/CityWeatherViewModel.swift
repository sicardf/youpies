//
//  CityWeatherViewModel.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import UIKit

class CityWeatherViewModel: NSObject {
    
    private var _cityWeatherViewModelItem: CityWeatherViewModelItem? {
        didSet {
            self.listCityWeatherDidChange?(self)
        }
    }
    public var cityWeatherViewModelItem: CityWeatherViewModelItem? {
        get {
            return _cityWeatherViewModelItem
        }
    }
    
    var listCityWeatherDidChange: ((CityWeatherViewModel) -> ())?
    
    init(city: String) {
        super.init()
        
        request(city: city)
    }
    
    private func request(city: String) {
        API().getWeather(city: city) { (data, success) in
            if success {
                if let data = data {
                    let listWeatherModel = WeatherModel(data: data)
                    if let weather = listWeatherModel?.weather {
                        self.addWeather(weather: weather)
                    }
                }
            }
        }
    }
    
    private func addWeather(weather: WeatherItem) {
        if let humidity = weather.humidity,
            let tempMin = weather.tempMin,
            let tempMax = weather.tempMax,
            let temp = weather.temp,
            let pressure = weather.pressure {
            _cityWeatherViewModelItem = CityWeatherViewModelItem(humidity: humidity,
                                                                 tempMin: tempMin,
                                                                 tempMax: tempMax,
                                                                 temp: temp,
                                                                 pressure: pressure)
        }
    }
    
}

class CityWeatherViewModelItem {
    
    var humidity: Int
    var tempMin: Int
    var tempMax: Int
    var temp: Double
    var pressure: Int
    
    init(humidity: Int, tempMin: Int, tempMax: Int, temp: Double, pressure: Int) {
        self.humidity = humidity
        self.tempMin = tempMin
        self.tempMax = tempMax
        self.temp = temp
        self.pressure = pressure
    }
}
