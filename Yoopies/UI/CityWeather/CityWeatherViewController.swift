//
//  CityWeatherViewController.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import UIKit

class CityWeatherViewController: UIViewController {

    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var tempMinLabel: UILabel!
    @IBOutlet weak var tempMaxLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    
    private var _city: CityViewModelItem!
    public var city: CityViewModelItem {
        get {
            return _city
        }
        set {
            _city = newValue
        }
    }
    
    private var viewModel: CityWeatherViewModel! {
        didSet {
            self.viewModel.listCityWeatherDidChange = { [unowned self] viewModel in
                self.display()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = _city.description
        contentView.isHidden = true
        viewModel = CityWeatherViewModel(city: _city.description)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func display() {
        if let cityWeather = viewModel.cityWeatherViewModelItem {
            tempLabel.text = "Temp : \(cityWeather.temp)"
            tempMinLabel.text = "TempMin : \(cityWeather.tempMin)"
            tempMaxLabel.text = "TempMax : \(cityWeather.tempMax)"
            humidityLabel.text = "Humidity : \(cityWeather.humidity)"
            pressureLabel.text = "Pressure : \(cityWeather.pressure)"
            contentView.isHidden = false
        }
    }
    
}
