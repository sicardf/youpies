//
//  AddCityViewController.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import UIKit

class AddCityViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    private var debouncedSearch: Debouncer!
    private var viewModel: AddCityViewModel! {
        didSet {
            self.viewModel.listCityDidChange = { [unowned self] viewModel in
                self.tableViewStyleLine(viewModel.listCityViewModelItem.count)
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Return", style: .plain, target: self, action: #selector(AddCityViewController.returnNavigationItem))
        viewModel = AddCityViewModel()
        initDebouncer()
        configTableView()
        configSearchBar()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func initDebouncer() {
        debouncedSearch = Debouncer(delay: 0.15) {
            if let text = self.searchBar.text {
                self.viewModel.request(city: text)
            }
        }
    }
    
    private func configTableView() {
        tableView?.dataSource = viewModel
        tableView?.delegate = self
        tableView?.keyboardDismissMode = .onDrag
        tableView?.estimatedRowHeight = 100
        tableView?.rowHeight = UITableViewAutomaticDimension
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: "cityCell")
        tableView?.separatorStyle = .none
        
        let messageTB = UILabel(frame: CGRect(x: 0, y: 0, width: (tableView?.bounds.size.width)!, height: (tableView?.bounds.size.height)!))
        messageTB.text = "Merci de faire une recherche"
        messageTB.textAlignment = .center
        messageTB.sizeToFit()
        tableView?.backgroundView = messageTB
    }
    
    private func configSearchBar() {
        searchBar.delegate = self
    }
    
    @objc func returnNavigationItem() {
        self.dismiss(animated: true) {}
    }

}

extension AddCityViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        DataCity().save(description: viewModel.listCityViewModelItem[indexPath.row].description)
        self.dismiss(animated: true) {}
    }
    
    func tableViewStyleLine(_ countItem: Int) {
        if countItem == 0 {
            self.tableView?.separatorStyle = .none
            tableView?.backgroundView?.isHidden = false
        } else {
            self.tableView.separatorStyle = .singleLine
            tableView?.backgroundView?.isHidden = true
        }
    }
}

extension AddCityViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        debouncedSearch.call()
    }
    
}
