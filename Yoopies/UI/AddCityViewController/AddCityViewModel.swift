//
//  AddCityViewModel.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import UIKit

class AddCityViewModel: NSObject {
    
    private var _listCityViewModelItem = [CityViewModelItem]() {
        didSet {
            self.listCityDidChange?(self)
        }
    }
    public var listCityViewModelItem: [CityViewModelItem] {
        get {
            return _listCityViewModelItem
        }
    }

    var listCityDidChange: ((AddCityViewModel) -> ())?
    
    func request(city: String) {
        _listCityViewModelItem = []
        API().getCity(city: city) { (data, success) in
            if success {
                if let data = data {
                    let cityModel = CityModel(data: data)
                    if let listCity = cityModel?.listCity {
                        self.addCity(listCity: listCity)
                    }
                }
            }
        }
    }
    
    private func addCity(listCity: [CityItem]) {
        for item in listCity {
            if let name = item.description {
                let city = CityViewModelItem(description: name)
                self._listCityViewModelItem.append(city)
            }
        }
    }
    
}

extension AddCityViewModel: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCityViewModelItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cityCell") as UITableViewCell!
        cell.textLabel?.text = listCityViewModelItem[indexPath.row].description
        return cell
    }
    
}

class CityViewModelItem {
    
    var description: String
    
    init(description: String) {
        self.description = description
    }
}
