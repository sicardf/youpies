//
//  CityModel.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import Foundation
import SwiftyJSON

class CityModel {

    var listCity = [CityItem]()
    
    init?(data: Data) {
        do {
            let json = try JSON(data: data)
            for item in json["predictions"] {
                let city = CityItem(json: item.1)
                listCity.append(city)
            }
        } catch {
            print("Error deserializing JSON: \(error)")
            return
        }
    }
    
}

class CityItem {
    var description: String?
    
    init(json: JSON) {
        self.description = json["description"].string
    }
}
