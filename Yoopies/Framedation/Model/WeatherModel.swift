//
//  ListWeatherModel.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import Foundation
import SwiftyJSON

class WeatherModel {

    var weather: WeatherItem?
    
    init?(data: Data) {
        do {
            let json = try JSON(data: data)
            weather = WeatherItem(json: json)
        } catch {
            return
        }
    }
    
}

class WeatherItem {
    
    var humidity: Int?
    var tempMin: Int?
    var tempMax: Int?
    var temp: Double?
    var pressure: Int?
    
    init(json: JSON) {
        self.humidity = json["main"]["humidity"].int
        self.tempMin = json["main"]["temp_min"].int
        self.tempMax = json["main"]["temp_max"].int
        self.temp = json["main"]["temp"].double
        self.pressure = json["main"]["pressure"].int
    }
    
}
