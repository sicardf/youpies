//
//  DataCity.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import UIKit
import CoreData

class DataCity {
    
    var appDelegate: AppDelegate!
    var managedContext: NSManagedObjectContext!
    //var entity: NSEntityDescription!
    
    var city: [NSManagedObject]!
    
    init() {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        managedContext = appDelegate.persistentContainer.viewContext
    //    entity = NSEntityDescription.entity(forEntityName: "City", in: managedContext)!
        
        city = []
    }
    
    func save(description: String) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let city = City(context: context)
        
        city.descriptionName = description
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
    }
    
    func delete(description: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        fetchRequest.predicate = NSPredicate(format: "descriptionName = %@", description)
        
        do{
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
            }
        } catch let error{
            print(error)
        }
    }
    
    func allCity() -> [City]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "City")
        
        do{
            let results = try managedContext.fetch(fetchRequest) as! [City]
            return results
        } catch let error{
            print(error)
        }
        return nil
    }
    
}
