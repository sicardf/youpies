//
//  API.swift
//  Yoopies
//
//  Created by Flavien SICARD on 2/1/18.
//  Copyright © 2018 Flavien SICARD. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    private func valueForAPIKey(named keyname:String) -> String {
        let filePath = Bundle.main.path(forResource: "APIKey", ofType: "plist")
        let plist = NSDictionary(contentsOfFile:filePath!)
        let value = plist?.object(forKey: keyname) as! String
        return value
    }
    
    func getWeather(city q: String, completion: @escaping (_ data: Data?, _ success: Bool) -> ()) {
            Alamofire.request("https://api.openweathermap.org/data/2.5/weather",
                              parameters: ["q": q,
                                           "appid": valueForAPIKey(named: "OWM_KEY"),
                                           "units": "metric"])
                .response { response in
                if let data = response.data {
                    completion(data, true)
                }
                completion(nil, false)
        }
    }
    
    func getCity(city input: String, completion: @escaping (_ data: Data?, _ success: Bool) -> ()) {
        Alamofire.request("https://maps.googleapis.com/maps/api/place/autocomplete/json",
                          parameters: ["input": input,
                                       "language": "fr",
                                       "types": "(cities)",
                                       "key": valueForAPIKey(named: "GOOGLE_KEY")])
            .response { response in
                if let data = response.data {
                    completion(data, true)
                }
                completion(nil, false)
        }
    }
    
}
